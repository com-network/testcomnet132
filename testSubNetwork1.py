import ipaddress


def calculate_subnet(ip_address, subnet_mask):
    network = ipaddress.IPv4Network(f"{ip_address}/{subnet_mask}", strict=False)
    network_address = str(network.network_address)
    broadcast_address = str(network.broadcast_address)
    num_addresses = network.num_addresses

    # Calculate Host Address Range
    host_range_start = str(network.network_address + 1)
    host_range_end = str(network.broadcast_address - 1)

    # Calculate Subnet ID
    subnet_id = str(network.network_address)

    # Calculate Subnet Bitmap
    subnet_bitmap = []
    for octet in network.network_address.packed:
        binary_octet = bin(octet)[2:].zfill(8)
        subnet_bitmap.append(binary_octet)
    subnet_bitmap = ".".join(subnet_bitmap)

    return {
        "Network Address": network_address,
        "Subnet ID": subnet_id,
        "Broadcast Address": broadcast_address,
        "Host Address Range": f"{host_range_start} - {host_range_end}",
        "Subnet Bitmap": subnet_bitmap,
        "Number of Host Addresses": num_addresses - 2
    }

if __name__ == "__main__":
    print("Subnet Calculator")
    ip_address = input("Enter an IP address (e.g., 192.168.1.1): ")
    subnet_mask = input("Enter a subnet mask (e.g., 255.255.255.0 or /24): ")

    try:
        results = calculate_subnet(ip_address, subnet_mask)
        for key, value in results.items():
            print(f"{key}: {value}")
    except ValueError as e:
        print(f"Error: {e}")
